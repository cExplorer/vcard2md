# vcard2md

Converting vcard file to md file that contains name, tel and adress.

## Install

* Make a git clone `git clone https://codeberg.org/cExplorer/vcard2md.git`
* Install module `pip install vobject`

## Usage

* cd `vcard2md`
* `./vcard2md.py -i my_vcard_file.vcf`
