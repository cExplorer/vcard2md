#!/usr/bin/python3
# -*- coding: utf-8 -*-

# This script converts a given vcard file to a md file
# It uses the vobject lib:
# http://eventable.github.io/vobject/
# https://github.com/eventable/vobject
# pip install vobject
# Spec of vcard file are found here:
# https://www.evenx.com/vcard-3-0-format-specification
# Thanx for accessing vcard items:
# https://stackoverflow.com/questions/35825919/vcard-parser-with-python
# Thanx for sorting list with dict value:
# https://www.w3schools.com/python/showpython.asp?filename=demo_ref_list_sort5
#
# Author:
# https://codeberg.org/cExplorer
#
# usage:
# ./vcard2md.py -i my_vcard_file.vcf
#

import sys
import os
import argparse
import vobject

# show debug messages
debug_mod = False

def show_debug_message(debugMessage):
    """show message """
    if debug_mod == True:
        print(debugMessage)

def list_sort(e):
    """for sorting list according dict item"""
    return e['vcf_name']


def write_md(list_vcards, file_vcard):
    """write md file"""
    file_name, file_extension = os.path.splitext(file_vcard)
    file_out = file_name + ".md"

    try:
        f_file_out = open(file_out, 'w')
    except IOError as e:
        errno, strerror = e.args
        sys.exit()

    f_file_out.write("<table>\n")
    f_file_out.write("<tr>")
    f_file_out.write("<th>Name</th>")
    f_file_out.write('<th colspan="2">Telefon</th>')
    f_file_out.write("<th>Adresse</th>")
    f_file_out.write("</tr>\n")

    for item in list_vcards:
        print(item.get("vcf_name"))
        tel = item.get("vcf_tel")
        adr = item.get("vcf_adr")

        if str(tel[0]) == "N":
            tel_0 = ""
        else:
            tel_0 = str(tel[0])

        print(tel_0)

        if len(tel) > 1:
            tel_1 = str(tel[1])
        else:
            tel_1 = ""

        print(tel_1)

        adr_0 = str(adr[0])
        if str(adr[0]) == "N":
            adr_0 = ""
        else:
            # remove coamma at the end of adress
            if adr_0[len(adr_0) - 3:len(adr_0) - 2] == ",":
                adr_0 = adr_0[:len(adr_0) - 3]

        print(adr_0)
        print("")

        f_file_out.write("<tr>")
        f_file_out.write(
            "<td>" + item.get("vcf_name") + "</td>"
            + "<td>" + tel_0 + "</td>"
            + "<td>" + tel_1 + "</td>"
            + "<td>" + adr_0 + "</td>")
        f_file_out.write("</tr>\n")

    print("md file written: " + file_out)


def read_vcard(file_vcard):
    """read vcard file """

    list_vcards = []
    no_tel = None
    no_adr = None

    with open(file_vcard) as file_in:
        data_in = file_in.read()
        vc = vobject.readComponents(data_in)
        vo = next(vc, None)
        while vo is not None:
            if debug_mod == True:
                vo.prettyPrint()

            try:
                show_debug_message(vo.contents['n'][0].value)
            except KeyError as error:
                show_debug_message("Key not found: %s" % error)

            show_debug_message(vo.contents['fn'][0].value)

            try:
                for tel in vo.contents['tel']:
                    show_debug_message(tel)
            except KeyError as error:
                show_debug_message("Key not found: %s" % error)
                no_tel = True

            try:
                for adr in vo.contents['adr']:
                    show_debug_message(adr)
            except KeyError as error:
                show_debug_message("Key not found: %s" % error)
                no_adr = True

            if no_tel is None and no_adr is None:
                list_vcards.append(
                    {"vcf_name": vo.contents['fn'][0].value,
                     "vcf_tel": [tel.value for tel in vo.contents['tel']],
                     "vcf_adr": [adr.value for adr in vo.contents['adr']]})

            if no_tel is None and no_adr is True:
                list_vcards.append(
                    {"vcf_name": vo.contents['fn'][0].value,
                     "vcf_tel": [tel.value for tel in vo.contents['tel']],
                     "vcf_adr": "None"})

            if no_tel is True and no_adr is None:
                list_vcards.append(
                    {"vcf_name": vo.contents['fn'][0].value,
                     "vcf_tel": "None",
                     "vcf_adr": [adr.value for adr in vo.contents['adr']]})

            no_tel = None
            no_adr = None
            vo = next(vc, None)

        list_vcards.sort(key=list_sort)
        show_debug_message(list_vcards)
        return list_vcards


if __name__ == '__main__':
    show_debug_message("\nLet's go")
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', help='vcard file')
    args = parser.parse_args()

    if args.i:
        show_debug_message(args.i)
        file_vcard = args.i

    print("")
    print("Found items:")
    print("")
    list_vcards = read_vcard(file_vcard)
    write_md(list_vcards, file_vcard)
    print("")
    show_debug_message("Let's go home")
